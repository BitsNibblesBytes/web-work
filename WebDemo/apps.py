from django.apps import AppConfig

class WebDemo(AppConfig):
    name = 'WebDemo'
    verbose_name = "Web Demo"
    module = 'models'
    def __init__(self):
         super(WebDemo,self).__init__("WebDemo",'models')