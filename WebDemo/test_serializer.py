from datetime import datetime

from rest_framework.response import Response
from rest_framework import serializers
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer, HTMLFormRenderer
from rest_framework.views import APIView

from WebDemo.models import Dataserver


class Comment(object):
    def __init__(self, email, content, created=None):
        self.email = email
        self.content = content
        self.created = created or datetime.now()



class CommentSerializer(serializers.Serializer):
    email = serializers.EmailField()
    content = serializers.CharField(max_length=200)
    created = serializers.DateTimeField()

#API returns list of database names that match
class DBLookup(serializers.Serializer):
    db_name = serializers.CharField(min_length=2,max_length=50)

    # How to process get vs post?


class DBList(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'dataserver.html'


    def get(self, request):
        # queryset = Dataserver.objects.all()
        # return Response({'dataservers': queryset})
        serializer = DBLookup()
        renderer = HTMLFormRenderer()
        form_html = renderer.render(serializer.data)
# comment = Comment(email='leila@example.com', content='foo bar')
# serializer = CommentSerializer(comment)
# json = JSONRenderer().render(serializer.data)