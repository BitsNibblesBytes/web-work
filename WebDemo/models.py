from django.db import models
import string
import random

class Dataserver(models.Model):
    name = models.CharField(unique=True,max_length=40)

    @property
    def databases(self):
        return self.database_set.all()

    def __str__(self):
        return self.name

class Database(models.Model):
    name = models.CharField(max_length=50)
    dataserver = models.ForeignKey(Dataserver,on_delete=models.CASCADE)



    class Meta:
        unique_together = ['name','dataserver']

    def __str__(self):
        return self.name


class User(models.Model):
    kerberos = models.CharField(unique=True,max_length=20)
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25)
    databases = models.ManyToManyField(Dataserver)

    def __str__(self):
        return self.kerberos



# Create Some Sample Data
def create_data_servers():
    num_servers = 100
    names = set()
    for x in range(num_servers):
        name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))
        while (name in names):
            name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))
        dataserver = Dataserver(name = name)
        dataserver.save()

    print "Names of all the servers:"
    for server in Dataserver.objects.all():
        print server

def create_databases():
    for server in Dataserver.objects.all():
        names = set()
        num_dbs = random.randrange(1,10)
        for x in range(num_dbs):
            name = ''.join(random.choice(string.ascii_uppercase) for _ in range(5))
            while (name in names):
                name = ''.join(random.choice(string.ascii_uppercase) for _ in range(5))
            db = Database(name=name,dataserver=server)
            db.save()
            # Should get error if we create duplicate...

    print "Names of all the servers & dbs:\n"
    for server in Dataserver.objects.all():
        print str(server)+":"
        for db in server.database_set.all():
            print "\t"+str(db)

