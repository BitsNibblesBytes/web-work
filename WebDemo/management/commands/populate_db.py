from django.core.management.base import BaseCommand
from WebDemo.models import *
# from ....models import *
# from WebDemo.models import Database


class Command(BaseCommand):
    args = '<foo bar ...>'
    help = 'our help string comes here'

    def handle(self, *args, **options):
        print "Herp Derp :D"
        create_data_servers()
        create_databases()